<!DOCTYPE html>
<html lang="pt-BR">
<head>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comes&Bebes</title>
    <?php wp_head(); ?>
</head>
<body>
    <!-- SIDEBAR MENU LATERAL SIDEBAR -->
    <div id="mySidenav" class="sidenav">

        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        
        <div id="box-carrinho">
            <h1 id = 'palavraCarrinho'>CARRINHO</h1>
        </div>
        <div id="box-produt-cart">
            <?php do_action('cart')?>
        </div>
        <div class="boxbotcomprar">
            <a id="botcomprar" href="http://projetofinalin.local/checkout/">COMPRAR</a>
        </div>

    </div>
    <!-- SIDEBAR MENU LATERAL SIDEBAR -->
    <header class = "headerNav">
        
        <div class = "alinhaRow">
            <a href="http://projetofinalin.local/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/logo.png" width = "77" height="50" id = "logoInicio"></a> 

            <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class = 'pesquisaHeader'>
                <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
                <button id = 'lupaHeader' type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/lupinha.png" id = 'dentroLupaHeader'></button>
                <input class = "busca" type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="" value="<?php echo get_search_query(); ?>" name="s" />
                <input type = "hidden" name="post_type" value="product" />

            
            </div>
            </form>

        </div>

        <div class = "iconesHeader alinhaRow">
            <a href = 'http://projetofinalin.local/shop/' id = "facaPedido">Faça um pedido</a>

            <!-- Use any element to open the sidenav -->
            <span onclick="openNav()"><div><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/carrinho.png" width = "48" height = "42" id = 'carrinhoHeader'></div></span>
                
            <a href="http://projetofinalin.local/my-account/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/user.png" width = "48" height = "42"></a>
        </div>

    </header>


    <!-- COMENTARIOS PARA MENU LATERAL EM CSS E PHP -->
            <!-- <div> -->
                <!-- <input type = 'checkbox' id= 'checkHeader'> -->
                <!-- <label id = 'lateralHeader' for = 'checkHeader'>  <img src="<'?'php echo get_stylesheet_directory_uri() ?>/assets/carrinho.png" width = "48" height = "42" id = 'carrinhoHeader </label>  -->
                <!-- <div class = 'conteudoLateral'>
                    <nav id = 'navLateral'>
                        <a href = ""><div class = "pedidoLateral">Teste</div></a>
                        <a href = ""><div class = "pedidoLateral">Teste2</div></a>
                    </nav>
                </div>  -->
            <!-- </div> --> 