<?php
	function my_scripts_carrinho(){
		wp_enqueue_script( 'carrinhoHeader', get_template_directory_uri().'/js/carrinho.js');
	}
	add_action( 'wp_enqueue_scripts', 'my_scripts_carrinho');
?>

<?
function cb_carrinho(){
        $total_value = 0;
        $items = WC()->cart->get_cart();
        foreach ($items as $item => $value){
            $_product = wc_get_product($value['data']->get_id());
            echo "<div class='produto' id = 'itensCarrinho'>
					<div id = 'imagemBonitaCarrinho'>". $_product->get_image() ."</div>
					<div class = 'produto-info'>
						<span class='text-left'>" . $_product->get_name() . "</span>
						<div class='produto-info2'<span>Quantidade: " . $value['quantity'] . "</span>
						" . wc_price($value['line_subtotal']) . "</div>
					</div>
            	 </div>";
            $total_value += $value['line_subtotal'];                
        }
        if($total_value != 0){
            echo "<span class='text-left' id = 'precoCarrinho'><br>Total do Carrinho: " . wc_price($total_value) . "</span>";
        }
    };
    add_action('cart','cb_carrinho');?>

