<?php /* Template Name: Landing-Page */ ?>
<div id = 'breu'></div>
<?php get_header(); ?>

        <main>
            
            <div id = 'inicio'>
                <h1 id = 'tituloHome'>Comes&Bebes</h1>
                <h3 id = 'textoHome'>O restaurante para todas as fomes</h3>
            </div>

            <div id = 'itensHome'>
                <h2 class = 'subtituloItens' id = 'centerHome'>CONHEÇA NOSSA LOJA</h2>
            </div>

            <h3 class = 'textosIndicandoHome'>Tipos de pratos principais</h3>

            <div class = 'categorias'>
                <?php 
                            $taxonomy     = 'product_cat';
                            $orderby      = 'name';  
                            $show_count   = 0;      // 1 for yes, 0 for no
                            $pad_counts   = 0;      // 1 for yes, 0 for no
                            $hierarchical = 1;      // 1 for yes, 0 for no  
                            $title        = '';  
                            $empty        = 1;
                        
                            $args = array(
                                'taxonomy'     => $taxonomy,
                                'orderby'      => $orderby,
                                'show_count'   => $show_count,
                                'pad_counts'   => $pad_counts,
                                'hierarchical' => $hierarchical,
                                'title_li'     => $title,
                                'hide_empty'   => $empty
                            );
                            $categories = get_categories($args);
                            if($categories){
                                foreach($categories as $category){
                                    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
                                    $image = wp_get_attachment_url($thumbnail_id);
                                    echo "<div class = 'categoriaComFoto'>
                                    <a href='". get_term_link($category->slug, 'product_cat')."'><img src='{$image}' alt='' class = 'imagemDaCategoria'></img></a>
                                    <p class = 'textoDaCategoria'>{$category->name}</p>
                                    </div>";
                                }
                            } 
                        ?>
                        
                    </div>
                
                
                
                
                
                
                    <h3 class="textosIndicandoHome"> Pratos do dia de hoje: </h3>
            
                <div id="diadasemana" class = 'textosIndicandoHome'>
                <?php   
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    $day=strftime(ucwords("%w",time()));
                    switch ($day) {
                        case 0:
                            echo "DOMINGO";
                            break;
                        case 1:
                            echo "SEGUNDA";
                            break;
                        case 2:
                            echo "TERÇA";
                            break;
                        case 3:
                            echo "QUARTA";
                            break;
                        case 4:
                            echo "QUINTA";
                            break;
                        case 5:
                            echo "SEXTA";
                            break;
                        case 6:
                            echo "SÁBADO";
                            break;
                        
                    }
                    ?>
                    </div>
                    <div class="pratossDoDia">
                    <?php 
                            
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        $day=strftime(ucwords("%a",time()));

                        $args = array(
                            'post_type'      => 'product',
                            'posts_per_page' => 4,
                            'product_tag'       => $day,
                        );
                    
                        $loop = new WP_Query( $args );
                    
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            $imagem = woocommerce_get_product_thumbnail();
                            $titulo = get_the_title();
                            $preco = wc_price($product->get_price_including_tax(1,$product->get_price()));;  
                            echo "<div class = 'pratoDoDiaFotoEPreco'>
                            <img {$imagem}</img>



                            <div id = 'faixaPratoDoDia'>
                                <p id = 'nomePratoDoDia'>{$titulo}</p>
                                <div id='precoDoDiaECarrinho'>
                                    <p>{$preco}</p> 
                                    <a href ='".get_permalink( )."'><img src='" . get_stylesheet_directory_uri() . "/assets/carroMais.png' width='48' height='43' id = 'botaoAddCarrinho'></a>
                                </div>
                            </div>
                            
                            </div>";
                            
                        endwhile;
                    
                        wp_reset_query();
                    ?>
                </div>
            </div>
            <div class = 'centralizaMeio'><a href = 'http://projetofinalin.local/shop/' id = 'botaoAmareloNoMeio'>Veja outras opções</a></div>

            <div id = 'dadosHome'>
                <h1 id = 'tituloBaixoHome'>VISITE NOSSA LOJA FÍSICA</h1>
                <div class = 'tentaFlex'>
                    <div id = 'mapaComInfoHome'>
                        <div id = "mapa"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1968390018756!2d-43.13644248503438!3d-22.906109485012156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1632958642432!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                        <div class = 'apenasFlex'>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/colherEFacaHomeEndereco.png" width = "19.12 " height="18.16" id = "colherEFaca">
                            <p class = 'textoBaixoHome'>Rua lorem ipsum, 123, LI, Brasil</p>
                        </div>
                        <div class = 'apenasFlex'>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/telHomeTelefone.png" width = "17.73" height="17.75" id = "telefoninho">
                            <p class = 'textoBaixoHome'>(XX) XXXX-XXXX</p>
                        </div>
                    </div>

                    <!-- SLIDES!!!! -->
                    <ul class="slider3">
                        <li>
                            <input type="radio" id="slide11" name="slidef" checked>
                            <label for="slide11"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/slideHome1.png" alt="slideIMG1">
                        </li>
                        <li>
                            <input type="radio" id="slide12" name="slidef" checked>
                            <label for="slide12"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/slideHome2.jpg" alt="slideIMG2">
                        </li>
                        <li>
                            <input type="radio" id="slide13" name="slidef" checked>
                            <label for="slide13"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/slideHome3.png" alt="slideIMG3">
                        </li>
            
                    </ul>
                </div>
            </div>
        </main>

<?php get_footer(); ?> 
